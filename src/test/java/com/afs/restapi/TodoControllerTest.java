package com.afs.restapi;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.entity.Todo;
import com.afs.restapi.repository.JPATodoRepository;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoUpdateRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.Random.class)
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @BeforeEach
    void clean() {
        jpaTodoRepository.deleteAll();
    }


    @Test
    void should_create_todo() throws Exception {
        TodoRequest todoRequest = getTodoRequestTest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJsonStr = objectMapper.writeValueAsString(todoRequest);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJsonStr))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoRequest.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_find_todos() throws Exception{
        Todo todoOne = jpaTodoRepository.save(getTodoByText("one"));
        Todo todoTwo = jpaTodoRepository.save(getTodoByText("two"));

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todoOne.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todoOne.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(getDoneState(todoOne.getDone())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(todoTwo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].text").value(todoTwo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(getDoneState(todoTwo.getDone())));
    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = jpaTodoRepository.save(getTodoByText("One"));

        mockMvc.perform(get("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(getDoneState(todo.getDone())));
    }

    @Test
    void should_throw_error_when_find_todo_by_id() throws Exception {
        Todo todo = jpaTodoRepository.save(getTodoByText("One"));
        MvcResult mvcResult = mockMvc.perform(get("/todos/{id}", todo.getId() + 1)).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        Assertions.assertEquals(404, response.getStatus());
    }

    @Test
    void should_update_todo_text_and_done() throws Exception {
        Todo todoOne = jpaTodoRepository.save(getTodoByText("One"));
        TodoUpdateRequest todoUpdateRequest = new TodoUpdateRequest();
        todoUpdateRequest.setText("Two");
        todoUpdateRequest.setDone(true);

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(todoUpdateRequest);
        mockMvc.perform(put("/todos/{id}", todoOne.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(200));

        Optional<Todo> optionalTodo = jpaTodoRepository.findById(todoOne.getId());
        assertTrue(optionalTodo.isPresent());
        Todo todo = optionalTodo.get();
        Assertions.assertEquals(todo.getId(), todoOne.getId());
        Assertions.assertEquals(todo.getText(), todoUpdateRequest.getText());
        Assertions.assertEquals(getDoneState(todo.getDone()), todoUpdateRequest.getDone());

    }

    @Test
    void should_delete_todo_by_id() throws Exception {
        Todo todoOne = jpaTodoRepository.save(getTodoByText("One"));

        mockMvc.perform(delete("/todos/{id}", todoOne.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));

        assertTrue(jpaTodoRepository.findById(todoOne.getId()).isEmpty());
    }

    private Todo getTodoByText(String text) {
        Todo todo = new Todo();
        todo.setText(text);
        return todo;
    }

    private TodoRequest getTodoRequestTest() {
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setText("test");
        return todoRequest;
    }

    private Boolean getDoneState(Integer state) {
        return state != 0;
    }

}
