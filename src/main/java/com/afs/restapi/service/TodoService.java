package com.afs.restapi.service;

import antlr.StringUtils;
import com.afs.restapi.entity.Todo;
import com.afs.restapi.exception.TodoNotFoundException;
import com.afs.restapi.repository.JPATodoRepository;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoResponse;
import com.afs.restapi.service.dto.TodoUpdateRequest;
import com.afs.restapi.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private final JPATodoRepository jpaTodoRepository;

    public TodoService(JPATodoRepository jpaTodoRepository) {
        this.jpaTodoRepository = jpaTodoRepository;
    }

    public TodoResponse create(TodoRequest todoRequest) {
        return TodoMapper.toResponse(jpaTodoRepository.save(TodoMapper.toEntity(todoRequest)));
    }

    public List<TodoResponse> getAll() {
        return TodoMapper.toResponses(jpaTodoRepository.findAll());
    }

    public TodoResponse getTodoResponseById(Long id) {
        return TodoMapper.toResponse(getTodoById(id));
    }

    public void update(Long id, TodoUpdateRequest todoUpdateRequest) {
        Todo todo = getTodoById(id);
        if (todoUpdateRequest.getText() != null && !todoUpdateRequest.getText().trim().equals("")) {
            todo.setText(todoUpdateRequest.getText());
        }
        if (todoUpdateRequest.getDone() != null) {
            todo.setDone(todoUpdateRequest.getDone() ? 1 : 0);
        }
        jpaTodoRepository.save(todo);
    }

    public Todo getTodoById(Long id) {
        return jpaTodoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }

    public void delete(Long id) {
        jpaTodoRepository.deleteById(id);
    }
}
