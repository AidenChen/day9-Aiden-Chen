package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        return CompanyMapper.toResponses(jpaCompanyRepository.findAll());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        return CompanyMapper.toResponses(jpaCompanyRepository.findAll(PageRequest.of(page - 1, size)).toList());
    }

    public CompanyResponse findById(Long id) {
        return CompanyMapper.toResponse(jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new));
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company toBeUpdatedCompany = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);;
        if (companyRequest != null && companyRequest.getName() != null) {
            toBeUpdatedCompany.setName(companyRequest.getName());
        }
        jpaCompanyRepository.save(toBeUpdatedCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        return CompanyMapper.toResponse(jpaCompanyRepository.save(CompanyMapper.toEntity(companyRequest)));
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        return EmployeeMapper.toResponses(jpaEmployeeRepository.findByCompanyId(id));
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
