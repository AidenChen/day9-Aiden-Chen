package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.hibernate.criterion.Example;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository, InMemoryEmployeeRepository inMemoryEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return EmployeeMapper.toResponses(jpaEmployeeRepository.findAll());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new));
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = jpaEmployeeRepository.findById(id).orElseThrow();
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        return EmployeeMapper.toResponses(jpaEmployeeRepository.findByGender(gender));
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        return EmployeeMapper.toResponse(jpaEmployeeRepository.save(employee));
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        return EmployeeMapper.toResponses(jpaEmployeeRepository.findAll(PageRequest.of(page - 1, size)).toList());
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
