package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Todo;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMapper {

    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        todoResponse.setDone(todo.getDone() != 0);
        return todoResponse;
    }

    public static List<TodoResponse> toResponses(List<Todo> todos) {
        return todos.stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }
}
