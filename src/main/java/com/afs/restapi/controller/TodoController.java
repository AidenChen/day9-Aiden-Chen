package com.afs.restapi.controller;

import com.afs.restapi.service.TodoService;
import com.afs.restapi.service.dto.TodoRequest;
import com.afs.restapi.service.dto.TodoResponse;
import com.afs.restapi.service.dto.TodoUpdateRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @PostMapping
    public TodoResponse createTodo(@RequestBody TodoRequest todoRequest) {
        return todoService.create(todoRequest);
    }

    @GetMapping
    public List<TodoResponse> getAllTodos() {
        return todoService.getAll();
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return todoService.getTodoResponseById(id);
    }

    @PutMapping("/{id}")
    public void updateTodo(@PathVariable Long id, @RequestBody TodoUpdateRequest todoUpdateRequest) {
        todoService.update(id, todoUpdateRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteTodo(@PathVariable Long id) {
        todoService.delete(id);
    }
}
