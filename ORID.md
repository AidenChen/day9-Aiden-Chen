## Objective
- Code View.
- Answer questions about React.
- Get an overview of what React learns with Concept Map.
- Practice front-end and back-end interactions.
- PPT display.

## Reflective
good!

## Interpretive
- I am more familiar with front-end and back-end interactions.
- Through the PPT display, I learned about user stories, elevator speeches, minimum available products and other concepts.

## Decisional
- Learn more about multi-point agile development.
